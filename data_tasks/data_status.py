#! /usr/bin/python3
# prep data status email for benchtools

from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import datetime
from datetime import date
from logins import *
from emailer import send_mail
import pprint

# set variables
data = {}
yesterday =  datetime.date.fromordinal(datetime.date.today().toordinal()-1).strftime("%F")
today = datetime.date.fromordinal(datetime.date.today().toordinal()).strftime("%F")


# get data for email from benchtools
for name, server in servers.items():
	# set up to run headless - so this can run as cronjob
	display = Display(visible = 0, size = (1600,900))
	display.start()

	# start browser
	browser = webdriver.Firefox()
	browser.get(server)
	browser.implicitly_wait(5)
	
	# log in to benchtools
	browser.find_element_by_xpath(".//*[@id='loginId']").send_keys(bt_username)
	browser.find_element_by_xpath(".//*[@id='password']").send_keys(bt_password + Keys.RETURN)
	
	# get sample status data
	browser.find_element_by_xpath(".//*[@id='topmenu']/div/ul[1]/li[3]/a").click()
	browser.find_element_by_xpath(".//*[@id='admin']/li[14]/a").click()
	sample_status = browser.find_element_by_xpath(".//*[@id='samples_table']/tbody/tr[2]").text
	sample_status = sample_status.split()

	# get keywords failed data
	browser.find_element_by_xpath(".//*[@id='topmenu']/div/ul[1]/li[3]/a").click()
	browser.find_element_by_xpath(".//*[@id='admin']/li[15]/a/i").click()
	keywords_failed = browser.find_element_by_xpath(".//*[@id='samples_table']/tbody").text
	keywords_failed = keywords_failed.splitlines()
	keywords_failed = [s for s in keywords_failed if yesterday in s] # get only data for yesterday
	
	# calculate files collected from sample status	
	parsed = int(sample_status[5])
	precald = int(sample_status[6])
	saved = int(sample_status[7])
	total = int(sample_status[9])
	files_collected = round((((parsed + precald + saved) / total) * 100), 2)
	
	# organize keywords failed and calculate total failed
	engines = {}
	total_keywords = 0
	total_failed = 0
	for engine_data in keywords_failed:
		print(engine_data)
		engine_data = engine_data.split()
		engine = engine_data[1]
		engine_keywords = int(engine_data[3])
		try:
			engine_failed = int(engine_data[4])
		except IndexError:
			engine_failed = 0
		engine_percent_failed = round(((engine_failed / engine_keywords) * 100), 2)
		engines[engine] = [engine_percent_failed, engine_keywords, engine_failed]	
		total_keywords += engine_keywords
		total_failed += engine_failed
	total_percent_failed = round(((total_failed / total_keywords) * 100), 2)

	# organize sampling and keyword data into dict
	data[name] ={'files_collected': files_collected, 'total_percent_failed': total_percent_failed, 'engines': engines}


# print data to log file
print(today)
pprint.pprint(data)


# set up email body html
summary_table = ""
for server, server_data in data.items():
	# set color for files colelcted and failed keywords based on performance benchmarks
	if (server_data['files_collected'] >= 95):
		files_color = '#006400'
	elif(server_data['files_collected'] < 85):
		files_color = 'red'
	else:
		files_color = '#ff8c00'
	if (server_data['total_percent_failed'] < 1):
		keywords_color = '#006400'
	elif(server_data['total_percent_failed'] >= 5):
		keywords_color = 'red'
	else:
		keywords_color = '#ff8c00'
	
	# build up html for files collected and keywords failed
	summary_table += "<table cellpadding='1'><tr><b>" + server + "</b></tr>"
	summary_table += "<br><tr><font color='" + files_color+ "'><b>" + str(server_data['files_collected']) + "% Files Collected</b></font></tr>"
	summary_table += "<br><tr><font color ='" + keywords_color+ "'><b>" + str(server_data['total_percent_failed']) + "% Failed Keywords</b></font></tr></table>"
	
	# build up html for failed by engine table
	engine_table = ""
	engine_table += r"<table cellpadding='1'><tr bgcolor='#00b2da'><th><font color='#ffffff'>Engine</font></th>"
	engine_table += r"<th><font color='#ffffff'>Total Keywords</font></th>"
	engine_table += r"<th><font color='#ffffff'>Failed Keywords</font></th>"
	engine_table += r"<th><font color='#ffffff'>Percent Failed</font></th></tr>"
	for engine_name, engine_data in server_data['engines'].items():
		engine_table += r"<tr><td>" + engine_name + "</td>"
		engine_table += r"<td align=RIGHT>" + str(engine_data[1]) + "</td>"
		engine_table += r"<td align=RIGHT>" + str(engine_data[2]) + "</td>"
		engine_table += r"<td align=RIGHT>" + str(engine_data[0]) + "%</td></tr>"
	engine_table += r"</table><br><br>"
	summary_table += engine_table

# lump all html for body of email together
body = "<html><body><h1>Collection</h1>" + summary_table + "<br><br><h1>Processing</h1><br><br><h1>Reporting</h1><br><br></body></html>"	


# send the email
subject = "Data Status " + today
send_mail(mail_server, port, email, password, recipient, subject, body)
