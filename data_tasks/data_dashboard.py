#! /usr/bin/python3
# open up data dashboard at beginning of day

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from logins import *

browser = webdriver.Firefox()
browser.maximize_window()

for name, server in servers.items():
	browser.get("localhost:8000")
	browser.implicitly_wait(5)
	browser.find_element_by_xpath(".//*[@id='loginId']").send_keys(bt_username)
	browser.find_element_by_xpath(".//*[@id='password']").send_keys(bt_password + Keys.RETURN)

	browser.find_element_by_xpath(".//*[@id='topmenu']/div/ul[1]/li[3]/a").click()
	browser.find_element_by_xpath(".//*[@id='admin']/li[22]/a").click()
	browser.find_element_by_xpath(".//*[@id='datetimepicker1']/span/span").click()
	browser.find_element_by_xpath(".//*[@id='run_dashboard']").click()
	browser.find_element_by_tag_name("body").send_keys(Keys.CONTROL + 't')