#! /usr/bin/python3
# send emails with python

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

# send mail 
def send_mail(server, port, email, password, recipient, subject, body):
	# connect and log in to mail server
	mail = smtplib.SMTP(server, port)
	mail.ehlo()
	mail.starttls()
	mail.login(email, password)
	
	# create email message
	message = MIMEMultipart('alternative')
	message['subject'] = subject
	message['From'] = email
	message['To'] = recipient	
	message_body = MIMEText(body, 'html')
	message.attach(message_body)

	# send that bitch and close mail server connection
	mail.sendmail(email, recipient, message.as_string())
	mail.quit()
