#! /usr/bin/python3
# check to see if reparse is really finished

from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import datetime
from datetime import date
from emailer import send_mail
from logins import *

# set variables
statuses = {}
servers = {'US' : 'http://benchtools.us'}

# log start time
print(datetime.datetime.now())


for name, server in servers.items():
	# log what server is running
	print(name)

	# set up headless - so we can cron this ish
	display = Display(visible = 0, size =(1600, 900))
	display.start()

	# start browser
	browser = webdriver.Firefox()
	browser.get(server)
	browser.implicitly_wait(5)

	# log in to benchtools
	browser.find_element_by_xpath(".//*[@id='loginId']").send_keys(bt_username)
	browser.find_element_by_xpath(".//*[@id='password']").send_keys(bt_password + Keys.RETURN)

	# get date reparse is running on
	browser.find_element_by_link_text("ADMIN").click()
	browser.find_element_by_link_text("Data Recovery").click()
	recovery_date = browser.find_element_by_xpath(".//*[@id='delete_table']/tbody/tr[1]/td[4]").text
	recovery_date = datetime.datetime.strptime(recovery_date, "%m/%d/%Y").strftime("%Y-%m-%d")

	# get sample status data
	browser.find_element_by_xpath(".//*[@id='topmenu']/div/ul[1]/li[3]/a").click()
	browser.find_element_by_xpath(".//*[@id='admin']/li[14]/a").click()
	sample_status = browser.find_element_by_xpath(".//*[@id='samples_table']/tbody").text.splitlines()
	sample_status = [s for s in sample_status if recovery_date in s]
	sample_status = [s.split(" ") for s in sample_status][0]
	saved = int(sample_status[6])
	parsed = int(sample_status[4])
	total = int(sample_status[8])
	percent_unparsed = saved / total
	percent_unprecald = parsed / total		
	if (percent_unparsed < .001 and percent_unprecald < .001):
		print("reparse finished")
		recovery_status = "completed"
	else:
		print("reparse still going")
		recovery_status = "ongoing"
	statuses[name] = recovery_status

if ("completed" in statuses.values()):
	print("somebody is done, send the email!")
	body = r"<html><body>" + str(statuses) + "</body></html>"
	subject = "Reparse Check"
	send_mail(mail_server, port, email, password, recipient, subject, body)
else:
	print("they still all going")

# log end time. cry over how slow sample status is
print(datetime.datetime.now())

