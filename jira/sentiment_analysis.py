#! /usr/bin/python3

import pprint
import requests 

def analyze_sentiment(text):
    headers = {
        "X-Mashape-Key": "ycXHTBEAgAmshKrnvcrLd8Esbs3ep1CjynSjsnHEdgtWfoRk6U",
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "application/json"
    }
    params = {
        "text": text
    }
    response = requests.post("https://twinword-sentiment-analysis.p.mashape.com/analyze/", headers=headers, params=params)
    response = response.json()
    return response['type']
