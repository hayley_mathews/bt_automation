#! /usr/bin/python3

# because the team could use some learning in history

from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from logins import jira_username, jira_password
import wikipedia
import random
from sentiment_analysis import analyze_sentiment

# make headless
display = Display(visible = 0, size = (1600, 900))
display.start()

# log in to jira
browser = webdriver.Firefox()
browser.get("https://benchtools.atlassian.net")
browser.find_element_by_xpath(".//*[@id='username']").send_keys(jira_username)
browser.find_element_by_xpath(".//*[@id='password']").send_keys(jira_password + Keys.RETURN)
browser.implicitly_wait(5)

# get all the tickets currently assigned to me for this sprint
tickets = browser.find_element_by_xpath(".//*[@id='gadget-10514']/div/div/table/tbody").text.splitlines()[::3]

for ticket in tickets:
	ticket_year = ticket.partition("-")[2]

	# check if the ticket already has a fun fact
	browser.get("https://benchtools.atlassian.net/browse/" + ticket)
	comments = browser.find_element_by_css_selector("#issue_actions_container").text
	
	# if no fun fact, go get one
	if 'Fun fact from ' + ticket_year not in comments:
		wiki_page = wikipedia.page(ticket_year).content.splitlines()
		
		# keep only the text between the "Events" and "Births" header
		start = wiki_page.index("== Events ==") + 1
		stop = wiki_page.index("== Births ==")
		events = wiki_page[start:stop]
		
		# don't want short facts. also don't want facts that are not so fun (the plague is not fun wikipedia)
		events = [s for s in events if len(s) > 50 and analyze_sentiment(s) != 'negative']
		random_event = "Fun Fact from " + ticket_year + ": " + random.choice(events)

		# then make jira more fun
		browser.find_element_by_xpath(".//*[@id='footer-comment-button']/span[2]").click()
		browser.find_element_by_xpath(".//*[@id='comment']").send_keys(random_event + Keys.RETURN)
